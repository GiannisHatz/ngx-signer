import { HttpClient, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { AngularDataContext } from '@themost/angular';
import { BehaviorSubject, Observable } from 'rxjs';
import 'rxjs/add/observable/of';
import { EventEmitter, Injectable } from '@angular/core';
import {ConfigurationService, ErrorService, ModalService} from '@universis/common';
import {ResponseError, TextUtils} from '@themost/client';

import { cloneDeep } from 'lodash';
import { SIGN_PARAMETERS } from './sign-parameters';
import {ServiceUrlPreProcessor} from '@universis/forms';
import {TranslateService} from '@ngx-translate/core';
import 'rxjs/add/operator/map';
import { VerificationCodeFormComponent } from './components/verification-code-form/verification-code-form.component';
import { BsModalService } from 'ngx-bootstrap/modal';

export interface SignatureInspectorConfiguration {
  text?: string[];
  textPosition?: string;
  width?: number;
  height?: number;
}

export interface SignerServiceConfiguration {
    timestampServer?: string;
    defaultLocation?: string;
    inspector?: SignatureInspectorConfiguration;
}


interface SettingsConfiguration {
    signer?: SignerServiceConfiguration;
}

export interface Certificate {
  version: number;
  subjectDN: string;
  sigAlgName: string;
  sigAlgOID: string;
  issuerDN: string;
  serialNumber: number;
  notAfter: string;
  notBefore: string;
  expired: boolean;
  thumbprint: string;
  commonName: string;
}

export interface VerifySignatureResult {
  valid: boolean;
  certificates: Certificate[];
  signatureProperties?: {
    singingDate: string;
    reason: string;
    signingCertificate: Certificate
  };
}

@Injectable()
export class SignerService {

    static SINGER_URI = 'http://localhost:2465/';
    static DEFAULT_SIGN_POSITION = '20,10,320,120';
    private _status: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    private _signerBasicAuthorization: string;
  private _schema: any;
  constructor(private _context: AngularDataContext,
              private _http: HttpClient,
              private _errorService: ErrorService,
              private _translateService: TranslateService,
              private _configurationService: ConfigurationService,
              private _modalService: BsModalService) {
    //
  }

  public get status(): BehaviorSubject<any> {
    setTimeout(() => {
        // call signer service and return value
    return this._http.get(SignerService.SINGER_URI).subscribe((response: HttpResponse<any>) => {
            this._status.next({
                ok: response.ok,
                status: response.status,
                statusText: response.statusText
            });
        }, (err: HttpErrorResponse) => {
            this._status.next({
                ok: err.ok,
                status: err.status,
                statusText: err.statusText
            });
        });
    }, 500);
    return this._status;
  }

  public authenticate({ username, password, rememberMe }: { username: string; password: string; rememberMe?: boolean; }): Promise<void> {
    const basicAuthorization = 'Basic ' + btoa(`${username}:${password}`);
    return this._http.get(new URL('/keystore/certs', SignerService.SINGER_URI).toString(), {
        headers: {
            'Authorization': basicAuthorization
        }
    }).toPromise().then(() => {
      if (rememberMe) {
        // set item to session storage
        sessionStorage.setItem('signer.auth', basicAuthorization);
      } else {
        // otherwise use memory
        this._signerBasicAuthorization = basicAuthorization;
      }
    });
  }
  public get authorization() {
    return sessionStorage.getItem('signer.auth') || this._signerBasicAuthorization;
  }
  /**
   * Gets a list of the available certificates
   */
  public getCertificates(): Promise<any> {

    if (this.authorization == null) {
      return Promise.reject(new ResponseError('Unauthorized', 401.5));
    }
    return this._http.get(new URL('/keystore/certs', SignerService.SINGER_URI).toString(), {
        headers: {
            'Accept': 'application/json',
            'Authorization': this.authorization
        },
        responseType: 'json'
    }).toPromise();
  }

    /**
     * Queries signer application status
     */
  public queryStatus(): Observable<any> {
      return new Observable( subscriber => {
          return this._http.get(SignerService.SINGER_URI).subscribe((response: HttpResponse<any>) => {
              subscriber.next({
                  ok: response.ok,
                  status: response.status,
                  statusText: response.statusText
              });
          }, (err: HttpErrorResponse) => {
              subscriber.next({
                  ok: err.ok,
                  status: err.status,
                  statusText: err.statusText
              });
          });
      });
  }

  public signDocument(blob: any, thumbprint: string, position?: string, image?: any, extras?: any): Promise<any> {
    // validate authentication
    if (this.authorization == null) {
      return Promise.reject(new ResponseError('Unauthorized', 401));
    }
    // get signer configuration
    const signerSettings = (<SettingsConfiguration>this._configurationService.settings).signer;
    // run signature inspection
    return (() => {
      // if position is already defined do nothing
      if (position != null) {
        return Promise.resolve();
      }
      if (blob.type === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
        return Promise.resolve();
      }
      // if inspector configuration is defined
      if (signerSettings && signerSettings.inspector != null) {
        // execute inspector
        return this.inspectPosition(blob, signerSettings.inspector);
      }
      // otherwise exit with no result
      return Promise.resolve();
    })().then((inspection: { page: number, position: Array<number> }) => {
      const formData = new FormData();
      if (extras != null) {
        Object.keys(extras).forEach((key) => {
          formData.append(key, extras[key]);
        });
      }
      // set certificate thumbprint
      formData.append('thumbprint', thumbprint);
      // append signature block inspection (only if position is null)
      if (position == null && inspection != null) {
        if (inspection.page) {
          formData.append('page', inspection.page.toFixed(0));
        }
        if (inspection.position) {
          formData.append('position', inspection.position.join(','));
        }
      }
      // finally set position
      if (formData.has('position') === false) {
        // set position or default
        formData.append('position', position || SignerService.DEFAULT_SIGN_POSITION);
      }
      let fileName = 'unsigned.pdf';
      if (extras && extras.fileName) {
        fileName = extras.fileName;
      }
      // set file
      formData.append('file', blob, fileName);
      // set image
      if (image) {
        formData.append('image', image, image.name);
      }
      // get timestamp server
      let timestampServer: string;
      if (signerSettings != null) {
        timestampServer = signerSettings.timestampServer;
      }
      // if timestamp server is defined
      if (timestampServer != null && timestampServer.length > 0) {
        formData.append('timestampServer', timestampServer);
      }
      // sign document
      return this._http.post(new URL('/sign', SignerService.SINGER_URI).toString(), formData, {
        headers: {
          'Authorization': this.authorization
        },
        responseType: 'blob',
        observe: 'response'
      }).toPromise().then( (response) => {
        return response.body;
      });
    });
  }

  public async replaceDocument(document: any, blob: Blob) {
    let item: any;
    if (Object.prototype.hasOwnProperty.call(document, 'id')) {
      item = await this._context.model('DocumentNumberSeriesItems').where('id').equal(document.id).getItem();
    } else if (Object.prototype.hasOwnProperty.call(document, 'url')) {
      item = await this._context.model('DocumentNumberSeriesItems').where('url').equal(document.url).getItem();
    }
    if (item == null) {
      throw new ResponseError('The specified document cannot be found', 404.4);
    }
    const replaceURL = this._context.getService().resolve(`DocumentNumberSeriesItems/${item.id}/replace`);
    const formData = new FormData();
    // set name
    formData.append('name', item.name);
    // set certificate thumbprint
    formData.append('contentType', item.contentType);
    formData.append('published', document.published);
    if (document.published) {
        formData.append('datePublished', TextUtils.escape(document.datePublished || new Date()).replace(/'/g, ''));
    }
    if (Object.prototype.hasOwnProperty.call(document, 'signed')) {
        formData.append('signed', document.signed);
    }
    // set file
    formData.append('file', blob, item.name);
    // sign document
    return this._http.post(replaceURL, formData, {
      headers: this._context.getService().getHeaders(),
      responseType: 'json',
      observe: 'response'
    }).toPromise().then( (response) => {
      return response.body;
    });
  }

    async getSignFormFor(document: any, extras?: any) {
        const form  = <any>cloneDeep(SIGN_PARAMETERS);
        new ServiceUrlPreProcessor(this._context).parse(form);
        form.params = form.params || {};
        let documentCanBePublished = true;
        if (extras && typeof extras.documentCanBePublished !== 'undefined') {
          documentCanBePublished = extras.documentCanBePublished;
        }
        Object.assign(form.params, {
          documentCanBePublished
        });
        const requiresUsernamePassword = await this.requiresUsernamePassword();
        Object.assign(form.params, {
          requiresUsernamePassword
        });
        Object.assign(form.params, document);
        return form;
    }

    setLastCertificate(thumbprint: string) {
        return sessionStorage.setItem('signer.lastCertificate', thumbprint);
    }

    getLastCertificate(): string {
        return sessionStorage.getItem('signer.lastCertificate');
    }

    destroy() {
      if (this._signerBasicAuthorization) {
          this._signerBasicAuthorization = null;
      }
    }

    protected async inspectPosition(blob: any, configuration: SignatureInspectorConfiguration): Promise<any> {
      const formData = new FormData();
      // todo this operation is going to be removed at next version
      try {
        await this._http.options(new URL('/signature/inspect', SignerService.SINGER_URI).toString()).toPromise();
      } catch (err) {
        if (err.status === 404) {
          console.log('Universis signer application is outdated. Please update universis signer to the latest version.');
        } else {
          console.log('Signer service failed to communicate with universis signer signature inspection.');
          console.log(err);
        }
      }
      // set name
      if (configuration.text != null) {
        configuration.text.forEach((value) => {
          formData.append('text', encodeURIComponent(value));
        });
      }
      if (configuration.textPosition != null) {
        formData.append('textPosition', configuration.textPosition);
      }
      if (configuration.width != null) {
        formData.append('width', configuration.width.toFixed(0));
      }
      if (configuration.height != null) {
        formData.append('height', configuration.height.toFixed(0));
      }
      // set file
      formData.append('file', blob, 'inspect-position.pdf');
      // sign document
      return this._http.post(new URL('/signature/inspect', SignerService.SINGER_URI).toString(), formData, {
        headers: this._context.getService().getHeaders(),
        responseType: 'json',
        observe: 'response'
      }).toPromise().then( (response) => {
        return response.body;
      });

    }

  /**
   * Gets openapi schema of universis signer
   */
  public getSchema(): Promise<{ info?: any,
    paths: any, externalDocs?: any, servers?: any[],
    components?: { schemas?: any, securitySchemes?: any } }> {
    if (this._schema != null) {
      return Promise.resolve(this._schema);
    }
    return this._http.get(new URL('/openapi/schema.json', SignerService.SINGER_URI).toString()).toPromise()
      .then((schema) => {
        this._schema = schema;
        return this._schema;
      }).catch((err) => {
        if (err.statusCode === 404) {
          return null;
        }
        if (err.status === 0) {
          return null;
        }
        return Promise.reject(err);
      });
  }

  async requiresVerificationCode() {
    // get signer schema
    const signerSchema = await this.getSchema();
    if (signerSchema == null) {
      return false;
    }
    // query schema required attributes
    const signEndpoint = signerSchema.paths['/sign'];
    if (signEndpoint == null) {
      throw new Error('Endpoint configuration cannot be empty at this context');
    }
    const signEndpointForm = signEndpoint.post.requestBody.content['multipart/form-data'];
    if (signEndpointForm == null) {
      throw new Error('Endpoint request configuration cannot be empty at this context');
    }
    const hasVerificationCode = signEndpointForm.schema.required.find((key: string) => {
      return key === 'otp' || key === 'verificationCode';
    });
    if (hasVerificationCode != null) {
      return true;
    }
    return false;
  }

  confirmVerificationCode(): Promise<any> {
    return new Promise((resolve, reject) => {
      let modalRef: any;
      const close = new EventEmitter();
      const verify = new EventEmitter();
      close.subscribe(() => {
        if (modalRef) {
          modalRef.hide();
        }
        return resolve(null);
      });
      verify.subscribe((verificationCode: any) => {
        if (modalRef) {
          modalRef.hide();
        }
        // verify here
        return resolve(verificationCode);
      });
      modalRef = this._modalService.show(VerificationCodeFormComponent, {
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          close: close,
          verify: verify
        }
      });
    });
  }

  async requiresUsernamePassword() {
    // get signer schema
    const signerSchema = await this.getSchema();
    if (signerSchema == null) {
      return false;
    }
    // query schema required attributes
    const certificatesEndpoint = signerSchema.paths['/keystore/certs'];
    if (certificatesEndpoint == null) {
      throw new Error('Endpoint configuration cannot be empty at this context');
    }
    // get basic auth scheme
    // { get: { security: [ { bacicAuth: [ "usernamePassword" ] } ] } }
    const basicAuthSecurity = certificatesEndpoint.get.security &&
      certificatesEndpoint.get.security.find((securityItem) => {
        return securityItem.basicAuth != null;
      });
    if (basicAuthSecurity == null) {
      return false;
    }
    const usernameAndPassword = basicAuthSecurity.basicAuth.find((scope: any) => {
      return scope === 'usernamePassword';
    });
    return usernameAndPassword != null;
  }

  public verifyDocument(file: any): Promise<VerifySignatureResult[]> {
    // create formData.
    const formData = new FormData();
    // set file.
    formData.append('file', file, file.name);
    // verify signature(s).
    return this._http.post<VerifySignatureResult[]>(new URL('/verify', SignerService.SINGER_URI).toString(), formData, {
      headers: {
          'Accept': 'application/json'
      },
      responseType: 'json'
    }).toPromise();
  }
}
